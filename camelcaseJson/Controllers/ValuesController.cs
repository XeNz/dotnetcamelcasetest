﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace camelCaseJson.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult Get()
        {
            // purpose is to get lowercased property keys in this jarray
            string str =
                "[{\"Photo\":null,\"OriginalPhoto\":null,\"LastName\":\"De Buyser\",\"OriginalLastName\":\"De Buyser\",\"FirstName\":\"Natascha\",\"OriginalFirstName\":\"Natascha\",\"Gender\":\"Vrouw\",\"OriginalGender\":\"Vrouw\",\"Insz\":\"75041932628\",\"OriginalInsz\":\"75041932628\",\"BirthDate\":\"1975-04-19T00:00:00Z\",\"OriginalBirthDate\":\"1975-04-19T00:00:00Z\",\"ResidentFrom\":\"2011-04-01T00:00:00Z\",\"OriginalResidentFrom\":\"2011-04-01T00:00:00Z\",\"MembershipType\":\"Titularis\",\"OriginalMembershipType\":\"Titularis\",\"Comments\":null},{\"Photo\":null,\"OriginalPhoto\":null,\"LastName\":\"Van Dingenen\",\"OriginalLastName\":\"Van Dingenen\",\"FirstName\":\"Ilana\",\"OriginalFirstName\":\"Ilana\",\"Gender\":\"Vrouw\",\"OriginalGender\":\"Vrouw\",\"Insz\":\"10030911272\",\"OriginalInsz\":\"10030911272\",\"BirthDate\":\"2010-03-09T00:00:00Z\",\"OriginalBirthDate\":\"2010-03-09T00:00:00Z\",\"ResidentFrom\":\"2011-04-01T00:00:00Z\",\"OriginalResidentFrom\":\"2011-04-01T00:00:00Z\",\"MembershipType\":\"Inwonend\",\"OriginalMembershipType\":\"Inwonend\",\"Comments\":null},{\"Photo\":null,\"OriginalPhoto\":null,\"LastName\":\"Van Dingenen\",\"OriginalLastName\":\"Van Dingenen\",\"FirstName\":\"Ilias\",\"OriginalFirstName\":\"Ilias\",\"Gender\":\"Man\",\"OriginalGender\":\"Man\",\"Insz\":\"06090516104\",\"OriginalInsz\":\"06090516104\",\"BirthDate\":\"2006-09-05T00:00:00Z\",\"OriginalBirthDate\":\"2006-09-05T00:00:00Z\",\"ResidentFrom\":\"2011-04-01T00:00:00Z\",\"OriginalResidentFrom\":\"2011-04-01T00:00:00Z\",\"MembershipType\":\"Inwonend\",\"OriginalMembershipType\":\"Inwonend\",\"Comments\":null}]";
            string str2 = string.Empty;


            //return Ok(JArrayToDtoMethod(str));
            //return Ok(SerializeDeserializeMethod(str));
            //return Ok(JsonDeserializeAsObjectWithCustomSettingsMethod(str));
            //return Ok(JTokenMethod(str));
            //return Ok(ExpandoObjectMethod(str));
            return Ok(ManualLoopOverPropertiesMethod(str));

            return Ok(null);
        }

        private static JArray ManualLoopOverPropertiesMethod(string json)
        {
            // WIP
            var o = JArray.Parse(json);
            foreach (JToken token in o)
            {
                Rename(token);
            }

            return o;
        }

        public static void Rename(JToken token)
        {
            //WIP
            var parent = token.Parent;
            if (parent == null)
                throw new InvalidOperationException("The parent is missing.");

            switch (token.Type)
            {
                case JTokenType.Array:
                    break;
                case JTokenType.Property:
                    var jProperty = token.ToObject<JProperty>();
                    var newJProperty = char.ToLowerInvariant(jProperty.Name[0]) + jProperty.Name.Substring(1);
                    var newToken = new JProperty(newJProperty, token);
                    parent.Replace(newToken);
                    break;
                case JTokenType.Object:
                    var jObject = token.ToObject<JObject>();
                    foreach (var keyValue in jObject)
                    {
                    }
                    break;
            }
        }

        private static object SerializeDeserializeMethod(string json)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            var obj = JsonConvert.DeserializeObject(json,
                settings);

            var converted = JsonConvert.SerializeObject(obj, settings);
            return JsonConvert.DeserializeObject(converted, settings);
        }

        private static object JsonDeserializeAsObjectMethod(string json)
        {
            var contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            };

            return JsonConvert.DeserializeObject<object>(json,
                new JsonSerializerSettings() {ContractResolver = contractResolver, Formatting = Formatting.Indented});
        }

        private static object JsonDeserializeAsObjectWithCustomSettingsMethod(string json)
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver =
                    new OverrideContractResolver(
                        new Dictionary<Type, IContractResolver>
                            {{typeof(object), new CamelCasePropertyNamesContractResolver()}},
                        new DefaultContractResolver()),
            };

            return JsonConvert.DeserializeObject<object>(json,
                settings);
        }

        private static object JArrayToDtoMethod(string json)
        {
            var jsonSerializer = new JsonSerializer() {ContractResolver = new CamelCasePropertyNamesContractResolver()};
            var obj = JArray.Parse(json).ToObject(typeof(IEnumerable<ClientInfo>), jsonSerializer);
            return obj;
        }

        private static JToken JTokenMethod(string json)
        {
            var token = JToken.Parse(json);

            switch (token)
            {
                case JArray array:
                    return array;
                case JObject o:
                    return o;
                default:
                    return null;
            }
        }

        private static string ExpandoObjectMethod(string json)
        {
            string myJsonOutput = string.Empty;
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            try
            {
                var interimObject = JsonConvert.DeserializeObject<ExpandoObject>(json);
                if (interimObject != null)
                {
                    myJsonOutput = JsonConvert.SerializeObject(interimObject, jsonSerializerSettings);
                }
            }
            catch (Exception)
            {
                //nothing
            }

            try
            {
                var interimObject = JsonConvert.DeserializeObject<IEnumerable<ExpandoObject>>(json);
                if (interimObject != null)
                {
                    myJsonOutput = JsonConvert.SerializeObject(interimObject, jsonSerializerSettings);
                }
            }
            catch (Exception)
            {
                //nothing
            }

            return myJsonOutput;
        }
    }

    public class ClientInfo
    {
        public object Photo { get; set; }
        public object OriginalPhoto { get; set; }
        public string LastName { get; set; }
        public string OriginalLastName { get; set; }
        public string FirstName { get; set; }
        public string OriginalFirstName { get; set; }
        public string Gender { get; set; }
        public string OriginalGender { get; set; }
        public string Insz { get; set; }
        public string OriginalInsz { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime OriginalBirthDate { get; set; }
        public DateTime ResidentFrom { get; set; }
        public DateTime OriginalResidentFrom { get; set; }
        public string MembershipType { get; set; }
        public string OriginalMembershipType { get; set; }
        public object Comments { get; set; }
    }


    public class OverrideContractResolver : ContractResolverDecorator
    {
        readonly Dictionary<Type, IContractResolver> overrides;

        public OverrideContractResolver(IEnumerable<KeyValuePair<Type, IContractResolver>> overrides,
            IContractResolver baseResolver)
            : base(baseResolver)
        {
            if (overrides == null)
                throw new ArgumentNullException();
            this.overrides = overrides.ToDictionary(p => p.Key, p => p.Value);
        }

        public override JsonContract ResolveContract(Type type)
        {
            IContractResolver resolver;
            if (overrides.TryGetValue(type, out resolver))
                return resolver.ResolveContract(type);
            return base.ResolveContract(type);
        }
    }

    public class ContractResolverDecorator : IContractResolver
    {
        readonly IContractResolver baseResolver;

        public ContractResolverDecorator(IContractResolver baseResolver)
        {
            if (baseResolver == null)
                throw new ArgumentNullException();
            this.baseResolver = baseResolver;
        }

        public virtual JsonContract ResolveContract(Type type)
        {
            return baseResolver.ResolveContract(type);
        }
    }
}